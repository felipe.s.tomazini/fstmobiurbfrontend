import {HttpClient, HttpClientModule } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { LinhaDeOnibus } from './shared/LinhaDeOnibus.model'

@Injectable()
export class OnibusService{


constructor(private http: HttpClient){

}

   public getLinhasDeOnibus(): Observable<LinhaDeOnibus[]> {

       return  this.http.get<LinhaDeOnibus[]>('http://www.poatransporte.com.br/php/facades/process.php?a=nc\&p=%&t=o')

  }
/*
  public getListLinhasDeOnibus(): Array<LinhaDeOnibus>{

    let response : LinhaDeOnibus[]
    let request : Observable<LinhaDeOnibus[]> = this.getLinhasDeOnibus()
    request.subscribe((linhas:LinhaDeOnibus[]) => {response = linhas; console.log(linhas); console.log(response)})

    return response
  }*/

}
