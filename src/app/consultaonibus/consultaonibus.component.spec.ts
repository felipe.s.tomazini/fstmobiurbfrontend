import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaonibusComponent } from './consultaonibus.component';

describe('ConsultaonibusComponent', () => {
  let component: ConsultaonibusComponent;
  let fixture: ComponentFixture<ConsultaonibusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultaonibusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaonibusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
