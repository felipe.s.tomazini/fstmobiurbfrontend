import { Component, OnInit, OnChanges } from '@angular/core';
import { waitForAsync } from '@angular/core/testing';
import { OnibusService} from '../Onibus.service'
import { LinhaDeOnibus } from '../shared/LinhaDeOnibus.model';

@Component({
  selector: 'app-consultaonibus',
  templateUrl: './consultaonibus.component.html',
  styleUrls: ['./consultaonibus.component.css'],

  providers: [OnibusService]
})
export class ConsultaonibusComponent implements OnInit {

  public linhas: LinhaDeOnibus[] = [];
  public linhasFiltradas: LinhaDeOnibus[] = [];
  public procurada : string = ""

  constructor(private onibusService: OnibusService) { }

  ngOnInit(): void {
    this.onibusService.getLinhasDeOnibus().subscribe((listaDeLinhas : LinhaDeOnibus[])=> {this.linhas = listaDeLinhas})

  }


  public clearVal() : void {
    this.procurada = "";
  }

  public searchItemProvider(searchItem : string) : void{

    this.procurada = searchItem;
    searchItem = ""
    this.procurada = this.procurada.toUpperCase()

  }


  public getfilterlist():void{
      this.linhasFiltradas = [];
      for(let i = 0; i < this.linhas.length ; i++){

        if (this.linhas[i].nome.search(this.procurada) != -1){
          this.linhasFiltradas.push(this.linhas[i]);

        }



      }

  }



}
