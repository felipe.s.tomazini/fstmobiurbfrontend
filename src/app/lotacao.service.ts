import {HttpClient, HttpClientModule } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { LinhaDeOnibus } from './shared/LinhaDeOnibus.model'

@Injectable()
export class LotacaoService{


constructor(private http: HttpClient){

}

   public getLinhasDeLotacao(): Observable<LinhaDeOnibus[]> {

       return  this.http.get<LinhaDeOnibus[]>('http://www.poatransporte.com.br/php/facades/process.php?a=nc\&p=%&t=l')

  }

}
