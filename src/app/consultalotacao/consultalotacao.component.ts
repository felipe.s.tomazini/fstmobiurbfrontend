import { Component, OnInit } from '@angular/core';
import { LotacaoService } from '../lotacao.service';
import { LinhaDeOnibus } from '../shared/LinhaDeOnibus.model';

@Component({
  selector: 'app-consultalotacao',
  templateUrl: './consultalotacao.component.html',
  styleUrls: ['./consultalotacao.component.css'],
  providers: [LotacaoService]
})
export class ConsultalotacaoComponent implements OnInit {

  public linhas: LinhaDeOnibus[] = [];
  public linhasFiltradas: LinhaDeOnibus[] = [];
  public procurada : string = ""

  constructor(private lotacaoService: LotacaoService) { }

  ngOnInit(): void {
    this.lotacaoService.getLinhasDeLotacao().subscribe((lotacoes:LinhaDeOnibus[]) => {this.linhas = lotacoes})

  }


  public clearVal() : void {
    this.procurada = "";
  }

  public searchItemProvider(searchItem : string) : void{

    this.procurada = searchItem;
    searchItem = ""
    this.procurada = this.procurada.toUpperCase()

  }


  public getfilterlist():void{
      this.linhasFiltradas = [];
      for(let i = 0; i < this.linhas.length ; i++){


        if (this.linhas[i].nome.search(this.procurada) != -1){
          this.linhasFiltradas.push(this.linhas[i]);

        }



      }

  }
}
