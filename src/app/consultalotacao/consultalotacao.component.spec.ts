import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultalotacaoComponent } from './consultalotacao.component';

describe('ConsultalotacaoComponent', () => {
  let component: ConsultalotacaoComponent;
  let fixture: ComponentFixture<ConsultalotacaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultalotacaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultalotacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
