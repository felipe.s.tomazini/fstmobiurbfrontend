import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopoComponent } from './topo/topo.component';
import { RodapeComponent } from './rodape/rodape.component';
import { HomeComponent } from './home/home.component';
import { ConsultaonibusComponent } from './consultaonibus/consultaonibus.component';
import { ConsultalotacaoComponent } from './consultalotacao/consultalotacao.component';
import { ItinerarioComponent } from './itinerario/itinerario.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { AgmCoreModule } from '@agm/core'

@NgModule({
  declarations: [
    AppComponent,
    TopoComponent,
    RodapeComponent,
    HomeComponent,
    ConsultaonibusComponent,
    ConsultalotacaoComponent,
    ItinerarioComponent,


  ],

  imports: [
   // AgmCoreModule.forRoot({apiKey : 'AIzaSyBFGPFXiXkxDdBuGCLCfyu6qVWCUIwB0vY'}),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule
  ],

  schemas: [CUSTOM_ELEMENTS_SCHEMA],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
