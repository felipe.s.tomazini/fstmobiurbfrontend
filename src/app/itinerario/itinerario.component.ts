import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ItinerarioService } from '../Itinerario.service';
import { GeoLocal } from '../shared/GeoLocal.model';
import { Itinerario } from '../shared/Itinerario.model';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-itinerario',
  templateUrl: './itinerario.component.html',
  styleUrls: ['./itinerario.component.css'],
  providers: [ItinerarioService]



})
export class ItinerarioComponent implements OnInit {

  itinerario : Itinerario = new Itinerario(-1,"","");
  pontos : GeoLocal[] = [];
  id : number = 0;



  constructor(private itinerarioService : ItinerarioService, private route : ActivatedRoute) { }

  ngOnInit(): void {

     this.route.params.subscribe((parametros: any)=> {this.id = parametros.id})
     this.itinerarioService.getItinerarioLinha(this.id)
    .subscribe((itinerario : any) =>
     {console.log(itinerario);
      this.itinerario.setIdLinhaItinerario(itinerario.idlinha);
      this.itinerario.setNomeLinhaItinerario(itinerario.nome);
      this.itinerario.setcodigoLinhaItinerario(itinerario.codigo);

      for(let i = 0; !!itinerario[i.toString()]; i++ ){

        let lat : string = itinerario[i.toString()].lat
        let lng : string = itinerario[i.toString()].lng
        let ponto : GeoLocal = new GeoLocal(i, lat, lng )
        this.pontos.push(ponto);

      }


      this.itinerario.setlistaPontos(this.pontos)
      console.log(this.itinerario)
    })





  }



}
