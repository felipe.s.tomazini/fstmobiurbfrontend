
import {GeoLocal} from '../shared/GeoLocal.model'


export class Itinerario{
  public idlinha: number
  public nome: string
  public codigo: string

  public pontos : Array<GeoLocal> = [];


  constructor( idlinha: number, codigo: string, nome: string){

        this.idlinha = idlinha;
        this.codigo = codigo;
        this.nome= nome;


  }

  public setIdLinhaItinerario(id : number) : void{
        this.idlinha = id;

  }
  public setcodigoLinhaItinerario(codigo : string) : void{
    this.codigo = codigo;

}
public setNomeLinhaItinerario(nome : string) : void{
  this.nome = nome;

}
public addPonto(ponto : GeoLocal) : void{
  this.pontos.push(ponto)

}

public setlistaPontos(pontos : GeoLocal[]) : void {
  this.pontos = pontos
}

}
