import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HttpClientModule } from '@angular/common/http'
import { BrowserModule } from '@angular/platform-browser';
import { ConsultaonibusComponent } from './consultaonibus/consultaonibus.component';
import { ConsultalotacaoComponent } from './consultalotacao/consultalotacao.component';
import { ItinerarioComponent } from './itinerario/itinerario.component';
import { MapaRotaComponent } from './mapa-rota/mapa-rota.component';
//import { AgmCoreModule } from '@agm/core';

const routes: Routes = [
  {path: '', component: ConsultaonibusComponent},
  {path: 'onibus', component: ConsultaonibusComponent},
  {path: 'lotacao', component: ConsultalotacaoComponent},
  {path: 'itinerario', component: ConsultalotacaoComponent},
  {path: 'itinerario/:id', component: ItinerarioComponent},
 // {path: 'mapa', component: MapaRotaComponent},

];

@NgModule({
  imports:
    [
  //    AgmCoreModule,
      RouterModule.forRoot(routes),
      HttpClientModule,
      BrowserModule
    ],
  exports: [RouterModule],


})
export class AppRoutingModule { }
